RED="\e[36m"
END="\e[0m"

echo -e "${RED}...................................${END}"
echo -e "${RED}|INFORMAÇÕES SOBRE A MÁQUINA ATUAL|${END}"
echo -e "${RED}...................................${END}"
sleep 3
echo " "
echo -e "${RED}Seu nome de usuário é:${END}" $(whoami)
sleep 2
echo "-------------------------------------------------------------------------------------------"

echo -e "${RED}Seu script está sendo executado no diretório:${END}" $(pwd)
sleep 2
echo "-------------------------------------------------------------------------------------------"

echo -e "${RED}Info de hora atual e tempo que o computador está ligado:${END}" $(uptime)
sleep 2
echo "-------------------------------------------------------------------------------------------"

echo -e "${RED}Quantidade de espaço de disco usado no seu sitema:${END}" 
df -h
sleep 2
echo "-------------------------------------------------------------------------------------------"

echo -e "${RED}Quantidade total de memória do sistema - Qtd. de memória livre - Qtd de memória em uso:${END}"
free -t
sleep 2
echo "-------------------------------------------------------------------------------------------"

echo -e "${RED}Status dos processos atuais:${END}"
ps
sleep 2
echo "-------------------------------------------------------------------------------------------"

echo -e "${RED}Informações sobre a cpu:${END}"
lscpu
sleep 2
echo "-------------------------------------------------------------------------------------------"
